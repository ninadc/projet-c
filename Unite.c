/* DE CASTRO, COMBE
 * TD2
 * Unite.c */

#include "Unite.h"

int creerUnite(char type, UListe *unite){
    UListe tmp;

    if (NULL == (tmp = malloc(sizeof(Unite))))
        return 0;
    tmp->genre = type;
    tmp->suiv = *unite;
    *unite = tmp;
    return 1;
}
