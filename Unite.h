/* DE CASTRO, COMBE
 * TD2
 * Unite.h */

#include <stdlib.h>

#define LONG 12
#define LARG 18
#define ROUGE 'R'
#define BLEU 'B'
#define SERF 's'
#define GUERRIER 'g'

typedef struct unite{
    int posX, posY;
    char couleur;
    char genre;
    struct unite *suiv;
}Unite, *UListe;

/* Description: Allocation et adressage d'un Unite
 * Paramètres: un pointeur sur une UListe et un char 'type'
 * Renvoie 0 en cas d'erreurs, 1 sinon */
int creerUnite(char type, UListe *unite);
