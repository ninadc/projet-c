/* DE CASTRO, COMBE
 * Actions.c */

#include "Graphic.h"
#include <stdio.h>
#include <ctype.h>

 int deplacerOuAttaquer(Unite *unite, Monde *monde, int destX, int destY){
     if (destX < 0 || destY < 0 || destX >= LARG || destY >= LONG)
         return -1;
     if (estVoisine(*unite, destX, destY) == 0){
 		printf("ESTVOIS DANS DEPLACER : %d\n", estVoisine(*unite, destX, destY));
         return -2;
 	}
     if (monde->plateau[destY][destX] == NULL){
         deplacerUnite(unite, monde, destX, destY);
         return 1;
     }
     if (unite->couleur == monde->plateau[destY][destX]->couleur && (dejaOccupee(monde->rouge, destX, destY) == 1 || dejaOccupee(monde->bleu, destX, destY) == 1))
         return -3;
     else{
         int res = attaquer(unite, monde, destX, destY);
         if (res == 1)
             return 2;
         else
             return 3;
     }
 }

void gereCoordonnees(UListe *tmp, Monde *monde){
    int x, y, res;

    printf("Veuillez saisir les coordonnées:\nX = ");
    while (scanf( "%d",&x) != 1) {
        printf("Veuillez saisir un chiffre:\nX = ");
        getchar(); // pour vider l'entrée
    }
    printf("Y = ");
    while (scanf( "%d",&y) != 1) {
        printf("Veuillez saisir un chiffre:\nY = ");
        getchar(); // pour vider l'entrée
    }
    if ((res = deplacerOuAttaquer((*tmp), monde, x, y)) < 0){
        switch (res){
            case -1: printf("ERR: La case saisie n'est pas valide\n\n"); break;
            case -2: printf("ERR: La case saisie n'est pas voisine\n\n"); break;
            case -3: printf("ERR: La case est déjà occupée par une unité alliée\n\n"); break;
            default: printf("ERR\n");
        }
    }
    else{
        affichePlateau(*monde);
    }
}

void gererDemiTour(char joueur, Monde *monde){
    UListe tmp;
    char choix = ' ';
    affichePlateau(*monde);

     if (joueur == ROUGE)
 		tmp = monde->rouge;
 	else
		tmp = monde->bleu;

    while (tmp != NULL){
        printf("\nJoueur %c: vous pouvez jouer le pion avec les caractéristiques suivantes: %c à la position (%d, %d)\nQue voulez vous faire ?\na: déplacer ou attaquer\nr: ne rien faire\nq: fin\nEntrez votre choix: ", joueur, tmp->genre, tmp->posX, tmp->posY);
        scanf(" %c", &choix);
        while( choix != 'a' && choix != 'r' && choix != 'q'){
            printf("Votre choix n'est pas valide, entrez un autre choix: ");
            scanf(" %c", &choix);
        }
        switch(choix){
            case 'a': gereCoordonnees(&tmp, monde); break;
            case 'q': return;
            case 'r': printf("Vous avec choisi de ne rien faire.\n"); break;
            default: break;
        }
        tmp = tmp->suiv;
		if (monde->rouge == NULL || monde->bleu == NULL){
			return;
		}
    }
    printf("Fin de votre tour\n");
}

 /* Laisse les deux joueurs jouer (toujours dans le même ordre) et qui met à jour le compteur de tours*/
 void gererTour(Monde *monde){
 	gererDemiTour(ROUGE, monde);
 	gererDemiTour(BLEU, monde);
 	monde->tour++;
 	printf("\n\nTOUR : %d\n\n", monde->tour);
 }

int randomInt(int nb){
     return rand()%(nb);
}

int createListe(UListe lst, Monde *monde, char couleur, int nb){
    int i;
    UListe u;

    for(i = 0; i < nb; i++){
		if(i < nb/2){
			if(creerUnite(GUERRIER, &u) == 0)
				return 0;
		}
		if(i >= nb/2){
			if(creerUnite(SERF, &u) == 0)
				return 0;
		}

        while (placerAuMonde(u, monde, randomInt(18), randomInt(12), couleur) == 0){}
    }
    return 1;
}

void gererPartie(void){
    Monde monde;
    char choix = ' ';
    initializerMonde(&monde);
    createListe(monde.rouge, &monde, ROUGE, 6);
    createListe(monde.bleu, &monde, BLEU, 6);

    do{
        printf("Voulez vous arrêter la partie ? O/N\n");
        scanf(" %c", &choix);
        while( choix != 'O' && choix != 'N'){
            printf("Votre choix n'est pas valide, entrez un autre choix: ");
            scanf(" %c", &choix);
        }
        if (choix == 'O'){
            printf("Vous arrêtez la partie, il n'y a donc pas de gagant, à bientôt!\n");
            viderMonde(&monde);
            return;
        }
        //choix = ' ';
        gererTour(&monde);
    }while(monde.rouge != NULL && monde.bleu != NULL);

    if (monde.rouge == NULL)
        printf("Le joueur BLEU a gagné !\n");
    else if (monde.bleu == NULL)
        printf("Le joueur ROUGE a gagné !\n");
    else
        printf("Égalité !\n");
    viderMonde(&monde);
}
