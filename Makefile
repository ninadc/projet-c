CFLAGS = -Wall -Werror
bin = bin/Main.o bin/Structures.o bin/Actions.o bin/Graphic.o

fight: Main.o Structures.o Actions.o Graphic.o
		gcc $(CFLAGS) -o fight $(bin)

Main.o: src/Main.c include/Actions.h
	gcc -o bin/$@ -c $< $(CFLAGS)

Structures.o: src/Structures.c include/Structures.h
	gcc -o bin/$@ -c $< $(CFLAGS)

Graphic.o: src/Graphic.c include/Graphic.h include/Structures.h
	gcc -o bin/$@ -c $< $(CFLAGS)

Actions.o: src/Actions.c include/Actions.h include/Graphic.h
	gcc -o bin/$@ -c $< $(CFLAGS)

clean:
	rm -Rf bin/*.o

mrproper: clean
	rm -f fight
