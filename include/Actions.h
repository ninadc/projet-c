/* DE CASTRO, COMBE
 * TD2
 * Actions.h */

#include "../include/Graphic.h"

 /* Description: gère les déplacements et les attaques
  * Paramètres: un pointeur sur un Unite, un pointeur sur un Monde et deux int 'destX' et 'destY'
  * Renvoie  1 si l'unité attaquante gagne, 0 si elle perd, -1 en cas d'erreurs */
 int deplacerOuAttaquer(Unite *unite, Monde *monde, int destX, int destY);

 /* Description: gère les coordonées saisies par un joueur
  * Paramètres: un pointeur sur UListe 'tmp, et un pointeur sur Monde 'monde'
  * Renvoie rien */
void gereCoordonnees(UListe *tmp, Monde *monde);

 /* Description: gère les actions d'un tour
  * Paramètres: un char 'joueur' et un pointeur sur un Monde
  * Renvoie rien */
 void gererDemiTour(char joueur, Monde *monde);

 /* Description: gère un tour complet de jeu
  * Paramètres: un pointeur sur un Monde
  * Renvoie rien */
 void gererTour(Monde *monde);

 /* Description: gère un tour complet de jeu
  * Paramètres: un pointeur sur un Monde
  * Renvoie rien */
 void gererTour(Monde *monde);

 /* Description: génération nombre aléatoire compris entre 0 et nb
  * Paramètres: un int nb
  * Renvoie un int */
int randomInt(int nb);

/* Description: génération d'une UListe aléatoire de taille nb
 * Paramètres: une UListe lst, un pointeur sur Monde 'monde', un char 'couleur' et un int 'nb'
 * Renvoie 0 en cas d'erreurs, 1 sinon */
int createListe(UListe lst, Monde *monde, char couleur, int nb);

 /* Description: effectue une partie du jeu
  * Paramètres: rien
  * Renvoie rien */
void gererPartie(void);
