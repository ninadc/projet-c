/* DE CASTRO, COMBE
 * TD2
 * Main.c */

#include "Graphic.h"
#include <stdio.h>

int main(int argc, char const *argv[]) {
    Monde monde;
    UListe u1, u2, u3, u4, u5;
    int err;
    creerUnite(SERF, &u1);
    creerUnite(GUERRIER, &u2);
    creerUnite(SERF, &u3);
    creerUnite(GUERRIER, &u4);
    creerUnite(GUERRIER, &u5);
    initializerMonde(&monde);
    placerAuMonde(u3, &monde, 3, 3, ROUGE);
    placerAuMonde(u1, &monde, 0, 0, BLEU);
    placerAuMonde(u2, &monde, 1, 1, ROUGE);
    placerAuMonde(u5, &monde, 5,5, ROUGE);
    placerAuMonde(u4, &monde, 4, 4, BLEU);
    afficheListe(monde.rouge);
    afficheListe(monde.bleu);
    affichePlateau(monde);
    /*deplacerUnite(u1, &monde, 2, 2);
    deplacerUnite(u2, &monde, 3, 2);
    attaquer(u3, &monde, 2, 2);*/
    err = deplacerOuAttaquer(u1, &monde, 1, 1);
    printf("err = %d\n", err);
    afficheListe(monde.rouge);
    printf("\n");
    afficheListe(monde.bleu);
    printf("\n");
    affichePlateau(monde);
	gererTour(&monde);
	gererTour(&monde);
	viderMonde(&monde);
	//gererDemiTour(ROUGE, &monde);
    return 0;
}
