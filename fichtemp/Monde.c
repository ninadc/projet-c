/* DE CASTRO, COMBE
 * TD2
 * Monde.c */

#include "Monde.h"
#include <stdio.h>


void initializerMonde(Monde *monde){
    int i, j;
    monde->tour = 0;

    for (i = 0; i < LONG; i++){
        for (j = 0; j < LARG; j++){
            monde->plateau[i][j] = NULL;
        }
    }
    monde->rouge = NULL;
    monde->bleu = NULL;
}

int dejaOccupee(UListe lst, int posX, int posY){
    UListe tmp = lst;
    while (tmp != NULL){
        if ((tmp->posX == posX) && (tmp->posY == posY))
            return 1;
        tmp = tmp->suiv;
    }
    return 0;
}

int placerAuMonde(Unite *unite, Monde *monde, int posX, int posY, char couleur){
    if (posX < 0 || posY < 0 || posX >= LARG || posY >= LONG)
        return 0;
    if (dejaOccupee(monde->rouge, posX, posY) == 1 || dejaOccupee(monde->bleu, posX, posY) == 1)
        return 0;
    unite->posX = posX;
    unite->posY = posY;
    unite->couleur = couleur;
    if (couleur == ROUGE){
        unite->suiv = monde->rouge;
        monde->rouge = unite;
    }
    else{
        unite->suiv = monde->bleu;
        monde->bleu = unite;
    }
    monde->plateau[posY][posX] = unite;
    return 1;
}

int uniteExist(UListe lst, Unite unite){
    UListe tmp = lst;

    while (tmp != NULL){
        if ((tmp->couleur == unite.couleur) && (tmp->genre == unite.genre) && (tmp->posX == unite.posX) && (tmp->posY == unite.posY))
            return 1;
        tmp = tmp->suiv;
    }
    return 0;
}

int deplacerUnite(Unite *unite, Monde *monde, int destX, int destY){
    int x, y;
    x = unite->posX;
    y = unite->posY;
    UListe tmp;
    if (uniteExist(monde->rouge, *unite) == 0 && uniteExist(monde->bleu, *unite) == 0)
        return 0;
    if (dejaOccupee(monde->rouge, destX, destY) == 1 || dejaOccupee(monde->bleu, destX, destY) == 1)
        return 0;

    if (unite->couleur == ROUGE)
        tmp = monde->rouge;
    else
        tmp = monde->bleu;

    while (tmp != NULL){
        if ((tmp->couleur == unite->couleur) && (tmp->genre == unite->genre) && (tmp->posX == unite->posX) && (tmp->posY == unite->posY)){
            tmp->posX = destX;
            tmp->posY = destY;
            unite->posX = destX;
            unite->posY = destY;
            monde->plateau[y][x] = NULL;
            monde->plateau[destY][destX] = unite;
            return 1;
        }
        tmp = tmp->suiv;
    }
    return 0;
}

int suppUniteListe(UListe *lst, Unite *unite, Monde *monde){
    UListe debut = *lst;
    if (*(lst) == NULL)
        return 0;
    if (((*lst)->couleur == unite->couleur) && ((*lst)->genre == unite->genre) && ((*lst)->posX == unite->posX) && ((*lst)->posY == unite->posY)){
        (*lst) = (*lst)->suiv;
        monde->plateau[unite->posY][unite->posX] = NULL;
        free(unite);
        return 1;
    }
    while ((*lst)->suiv != NULL){
        if (((*lst)->suiv->couleur == unite->couleur) && ((*lst)->suiv->genre == unite->genre) && ((*lst)->suiv->posX == unite->posX) && ((*lst)->suiv->posY == unite->posY)){
            (*lst)->suiv = (*lst)->suiv->suiv;
            monde->plateau[unite->posY][unite->posX] = NULL;
            *lst = debut;
            free(unite);
            return 1;
        }
        (*lst) = (*lst)->suiv;
    }
    return 0;
}

int enleverUnite(Unite *unite, Monde *monde){
    if (uniteExist(monde->rouge, *unite) == 0 && uniteExist(monde->bleu, *unite) == 0)
        return 0;
    if (unite->couleur == ROUGE)
        return suppUniteListe(&(monde->rouge), unite, monde);
    else
        return suppUniteListe(&(monde->bleu), unite, monde);
    return 0;
}

int attaquer(Unite *unite, Monde *monde, int posX, int posY){
    if (unite->genre == monde->plateau[posY][posX]->genre){
        enleverUnite(monde->plateau[posY][posX], monde);
        return 1;
    }
    if (unite->genre == GUERRIER && monde->plateau[posY][posX]->genre == SERF){
        enleverUnite(monde->plateau[posY][posX], monde);
        return 1;
    }
    if (unite->genre == SERF && monde->plateau[posY][posX]->genre == GUERRIER){
        enleverUnite(unite, monde);
        return 0;
    }
    return -1;
}

int estVoisine(Unite unite, int destX, int destY){
    /*int x = unite.posX;
    int y = unite.posY;
    if (X == x && Y == y-1)
        return 1;
    if (X == x && Y == y+1)
        return 1;
    if (X == x+1 && Y == y)
        return 1;
    if (X == x-1 && Y == y)
        return 1;*/
    if (destX >= unite.posX-1 && destX <= unite.posX+1 && destY >= unite.posY-1 && destY <= unite.posY+1)
        return 1;
    return 0;
}

/*
qui g`ere le d ́eplacements et le combat. La valeur de retour sert `a informer du r ́esultat.
— Si les coordonn ́ees ne sont pas valides, la fonction ne fait rien et retourne -1.
— Si la case marqu ́ee par les coordonn ́ees n’est pas voisine `a celle ou` l’unit ́e
en question se trouve, la fonction ne fait rien et retourne -2.
— Si la case marqu ́ee par les coordonn ́ees est d ́ej`a occup ́e par une unit ́e alli ́ee,
la fonction ne fait rien et retourne -3.
— Si la case destination est vide, valide et adjacente, l’unit ́e se d ́eplace, la fonction retourne 1.
— Si la case de destination est valide, adjacente, et occup ́ee par un ennemi, un combat prend lieu.
    La fonction retourne 2 si l’attaquant a gagn ́e ou 3 s’il a perdu.*/
int deplacerOuAttaquer(Unite *unite, Monde *monde, int destX, int destY){
    if (destX < 0 || destY < 0 || destX >= LARG || destY >= LONG)
        return -1;
    if (estVoisine(*unite, destX, destY) == 0){
		printf("ESTVOIS DANS DEPLACER : %d\n", estVoisine(*unite, destX, destY));
        return -2;
	}	
    if (monde->plateau[destY][destX] == NULL){
        deplacerUnite(unite, monde, destX, destY);
        return 1;
    }
    if (unite->couleur == monde->plateau[destY][destX]->couleur && (dejaOccupee(monde->rouge, destX, destY) == 1 || dejaOccupee(monde->bleu, destX, destY) == 1))
        return -3;
    else{
        int res = attaquer(unite, monde, destX, destY);
        if (res == 1)
            return 2;
        else
            return 3;
    }
}

/*Pour chacune d’eux :
— l’ ́etat actuel du plateau de jeu ansi que les informations concernant cette unit ́e sont
a ch ́es ;
— les ordres pour l’unit ́e sont clairement demand ́ees
(direction de d ́eplacement/attaque ou ne rien faire) ;
— l’ordre est e↵ectu ́ee et l’utilisateur est inform ́e du r ́esultat
(si l’ordre n’est pas valide – donc la fonction deplacerOuAttaquer renvoie une va- leur n ́egitive –
l’utilisateur en est inform ́e, mais ne re ̧coit pas l’opportunit ́e de se corriger ; l’unit ́e ne fera rien ce tour) ;*/
void gererDemiTour(char joueur, Monde *monde){
   /******************modification********************/
	char choix=' ';
    int x, y, res;
	UListe tmp;
	printf("%c\n", joueur);
    affichePlateau(*monde);
	
    if (joueur == ROUGE){
		//printf("rouge");
		tmp = monde->rouge;
	}
	else
		tmp = monde->bleu;
	
	while(choix !='q'){
		if(tmp != NULL){
			printf("Joueur %c: vous pouvez jouer le pion avec les caractéristiques suivante: %c à la position (%d, %d)", joueur, tmp->genre, tmp->posX, tmp->posY);
			printf("Que voulez vous faire ?\na: déplacer ou attaquer\nr: ne rien faire\nq: fin\nEntrez votre choix: ");
			scanf("%c", &choix);
			while( choix != 'a' && choix != 'r' && choix != 'q'){
				scanf("%c", &choix);
			}	

			if (choix == 'a'){
				printf("Saisir des coordonnées de destination\nX = ");
				scanf("%d", &x);
				printf("Y = ");
				scanf("%d", &y);
				if ((res = deplacerOuAttaquer((tmp), monde, x, y)) < 0){
					switch (res){
						case -1: printf("ERR: La case saisie n'est pas valide\n"); break;
						case -2: printf("ERR: La case saisie n'est pas voisine\n"); break;
						case -3: printf("ERR: La case est déjà occupée par une unité alliée\n"); break;
						default: printf("ERR\n");
					}
				}
				else{
					affichePlateau(*monde);
				}
			}
			else{
				printf("Vous avec choisi de ne rien faire.\n");
			}
			if(tmp->suiv != NULL && choix != 'q')
				printf("Pion suivant :\n");
			tmp=tmp->suiv;		
		}
		else{
			printf("Fin de votre tour\n");
			choix='q';
			//scanf("%c", &choix);
		}
	}
	printf("fin while q\n");
	/*
	//Vérif liste monde 
	if (joueur == ROUGE){
			while(monde->rouge != NULL){
				printf("monde rouge : %d %d\n", monde->rouge->posX, monde->rouge->posY);
				monde->rouge=monde->rouge->suiv;
			}
		}
	*/
}

/* Laisse les deux joueurs jouer (toujours dans le même ordre) et qui met à jour le compteur de tours*/
void gererTour(Monde *monde){
	gererDemiTour(ROUGE, monde);
	gererDemiTour(BLEU, monde);
	monde->tour++;
	printf("tour : %d\n", monde->tour);
}

/*Toute la mémoire allouée pour les unités sera libérée et le monde, en tant que structure sera réinitialisé (notamment tous ces pointeurs remis à NULL)*/
void viderMonde(Monde *monde){
	UListe tmpR = monde->rouge;
	UListe tmpB = monde->bleu;
	while(tmpR!=NULL){
		free(tmpR);
		tmpR= tmpR->suiv;
		printf("TEST R %d\n", monde->rouge->posX);
		affichePlateau(*monde);
	}
	while(tmpB!=NULL){
		free(tmpB);
		tmpB= tmpB->suiv;
		printf("TEST B %d\n", monde->bleu->posY);
		affichePlateau(*monde);
	}	
}
