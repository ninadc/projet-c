/* DE CASTRO, COMBE
 * TD2
 * Graphic.c */

#include "../include/Graphic.h"
#include <stdio.h>

void affichePlateau(Monde monde){
    int i, j;
	int k=0;

	for(j=0; j<LARG; j++){
		if(j==0)
			printf("   ");
		if(j<10)
			printf(" %d   ", j);
		else
			printf(" %d  ", j);
	}
    printf("\n  -------------------------------------------------------------------------------------------\n");
    for (i = 0; i < LONG; i++){

		if(k<10)
        	printf("%d |", k);
		else
			printf("%d|", k);

        for (j = 0; j < LARG; j++){
            if (monde.plateau[i][j] == NULL)
                printf("    |");
            else{
                printf(" %c%c |", monde.plateau[i][j]->couleur, monde.plateau[i][j]->genre);
			}
		}
		k++;
        printf("\n  -------------------------------------------------------------------------------------------\n");
    }
}

void afficheListe(UListe lst){
	UListe tmp = lst;

	while(NULL != tmp){
		printf("%c, %c, (%d,%d) -> ", tmp->couleur, tmp->genre, tmp->posX, tmp->posY);
		tmp = tmp->suiv;
	}
	printf("\n");
}
